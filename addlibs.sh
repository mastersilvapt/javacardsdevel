#!/bin/bash

#git clone https://gitlab.com/mastersilvapt/javacardsdevel /tmp/javacardsdevel && cd /tmp/javacardsdevel && chmod +x ./addlibs.sh && ./addlibs.sh && echo "Done"

tar xfv javacard.tar
tar xfv licel.tar

mv ~/.m2/repository/oracle/javacard ./javacard_OLD
mv ~/.m2/repository/com/licel ./licel_OLD

mv ./javacard ~/.m2/repository/oracle/.
mv ./licel ~/.m2/repository/com/.
